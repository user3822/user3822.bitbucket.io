const wordInput   = document.querySelector('#password-input');
const pwOutput    = document.querySelector('#password-output span');
const pwContainer = document.querySelector('.password-output-container');
const generateBtn = document.querySelector('.generate-btn');

function validate(input) {
  if (!input) {
    // invalid
    wordInput.animate(shake, {duration: 200});
  } else {
    // valid
    generateBtn.blur();
    if (!pwContainer.style.height) {
      pwOutput.innerText = 'TEMP';
      pwContainer.style.height = pwContainer.scrollHeight + 'px';
    }

  }
  return input;
}

const shake = [
  { transform: 'translateX(-1%)' },
  { transform: 'translateX(1%)' },
  { transform: 'translateX(-1%)' },
  { transform: 'translateX(1%)' }
];

generateBtn.addEventListener('click', generator);
wordInput.addEventListener('keyup', (event) => {
  const validKey = event.code === 'Enter' || event.code === 'NumpadEnter';
  if (validKey) {
    generator();
  }
});

pwContainer.addEventListener('click', () => {
  try {
    const pw = pwOutput.innerText;
    navigator.clipboard.writeText(pw);
    console.log(pwOutput);
    pwOutput.style.disabled = true;
    pwOutput.innerText = 'Copied!';
    pwOutput.style.opacity = 0;
    setTimeout(() => {
      pwOutput.innerText = pw;
      pwOutput.style.opacity = '';
      pwOutput.style.disabled = false;
    }, 500);
  } catch (err) {
    console.log(err);
  }
});

function generator() {
  /** @type {string} */
  const str = wordInput.value.replace(/\s/g, '');
  if (validate(str)) {
    let newPW = '';
    const r = Math.floor(Math.random() * wrapper.length);
    const wrap = wrapper[r];
    for (let i = 0; i < str.length; i++) {
      const result = map.get(str[i]);
      newPW += result ? result : str[i];
    }
    newPW = `${wrap}${newPW}${wrap}`;
    pwOutput.dataset.value = newPW;
    pwOutput.innerText = newPW;
  }
}

const map = new Map(
  [
    ['a', '@'],
    ['A', '@'],
    ['b', 'b'],
    ['B', '8'],
    ['c', 'c'],
    ['C', 'C'],
    ['d', 'd'],
    ['D', 'D'],
    ['e', 'e'],
    ['E', 'E'],
    ['f', 'f'],
    ['F', 'F'],
    ['g', '9'],
    ['G', '6'],
    ['h', 'h'],
    ['H', '#'],
    ['i', '1'],
    ['I', '1'],
    ['j', 'j'],
    ['J', 'J'],
    ['k', 'k'],
    ['K', 'K'],
    ['l', '7'],
    ['L', '7'],
    ['m', 'm'],
    ['M', 'M'],
    ['n', 'n'],
    ['N', 'N'],
    ['o', '0'],
    ['O', '0'],
    ['p', 'p'],
    ['P', 'P'],
    ['q', 'q'],
    ['Q', 'Q'],
    ['r', 'r'],
    ['R', 'R'],
    ['s', '$'],
    ['S', '$'],
    ['t', '+'],
    ['T', '+'],
    ['u', 'u'],
    ['U', 'U'],
    ['v', 'v'],
    ['V', 'V'],
    ['w', 'w'],
    ['W', 'W'],
    ['x', 'x'],
    ['X', 'X'],
    ['y', 'y'],
    ['Y', 'Y'],
    ['z', 'z'],
    ['Z', 'Z']
  ]
);

const wrapper = [
  '!',
  '@',
  '#',
  '$',
  '%',
  '^',
  '&',
  '*',
  '(',
  ')',
  '[',
  ']',
  '-',
  '_',
];